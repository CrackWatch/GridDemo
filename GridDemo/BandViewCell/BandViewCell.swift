//
//  BandViewCell.swift
//  GridDemo
//
//  Created by Lazy on 2018/3/27.
//  Copyright © 2018年 Lazy. All rights reserved.
//

import G3GridView

class BandViewCell: GridViewCell {

    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var endLabel: UILabel!
    @IBOutlet weak var bandNameLabel: UILabel!
    @IBOutlet weak var cardView: UIView!

    static var nib: UINib {
        return UINib(nibName: "BandViewCell", bundle: Bundle(for: self))
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        clipsToBounds = true
        startLabel.textAlignment = .left
        endLabel.textAlignment = .left
        bandNameLabel.textAlignment = .center
        
    }

    func configBandViewCell(model: Model) {
        switch model.isshow {
        case true?:
            cardView.backgroundColor = UIColor(0xFFFFFF)
        case false?:
            cardView.backgroundColor = UIColor.clear
        default:
            break
        }

        startLabel.text = model.start
        endLabel.text = model.end
        bandNameLabel.text = model.name
    }
}
