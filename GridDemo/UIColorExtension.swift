//
//  UIColorExtension.swift
//  WakeUpFest18
//
//  Created by MicroProgram_G6107009 on 2018/4/13.
//  Copyright © 2018年 Lazy. All rights reserved.
//

import UIKit

/**
 擴充UIColor
 支援從Hex色碼
 支援漸層顏色漸變取色
 */

import UIKit

extension UIColor {


    /// 從Hex色碼建立實體 , ARGB 模式
    ///
    /// - Parameter argb: 4 byte ARGB
    convenience init(argb: Int) {
        let red: CGFloat = CGFloat((argb >> 16) & 0xff) / 255.0
        let green: CGFloat = CGFloat((argb >> 8) & 0xff) / 255.0
        let blue: CGFloat = CGFloat(argb & 0xff) / 255.0
        let alpha: CGFloat = CGFloat((argb >> 24) & 0xFF) / 255.0
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }

    /**
     從Hex色碼建立實體
     
     @param netHex Hex色碼，例如0xFFFFFF
     @param alpha 不透明度0 ~ 1
     */
    convenience init(_ netHex: Int, alpha: CGFloat = 1.0) {

        let red: CGFloat = CGFloat((netHex >> 16) & 0xff) / 255.0
        let green: CGFloat = CGFloat((netHex >> 8) & 0xff) / 255.0
        let blue: CGFloat = CGFloat(netHex & 0xff) / 255.0

        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }

    /**
     漸變取色
     
     @param fraction 漸變程度0~1，0為顯示Start顏色 / 1為顯示End顏色
     @param start Start顏色
     @param start End顏色
     */
    static func evaluate(fraction: CGFloat, start: UIColor, end: UIColor) -> UIColor?
    {
        var startRed: CGFloat = 0
        var startGreen: CGFloat = 0
        var startBlue: CGFloat = 0
        var startAlpha: CGFloat = 0
        var endRed: CGFloat = 0
        var endGreen: CGFloat = 0
        var endBlue: CGFloat = 0
        var endAlpha: CGFloat = 0

        if start.getRed(&startRed, green: &startGreen, blue: &startBlue, alpha: &startAlpha) &&
            end.getRed(&endRed, green: &endGreen, blue: &endBlue, alpha: &endAlpha)
            {
            return UIColor.init(
                red: startRed + (fraction * (endRed - startRed)),
                green: startGreen + (fraction * (endGreen - startGreen)),
                blue: startBlue + (fraction * (endBlue - startBlue)),
                alpha: startAlpha + (fraction * (endAlpha - startAlpha)))
        }

        return nil
    }

    /// 將顏色轉為純色圖片
    func toImage(_ size: CGSize) -> UIImage? {

        UIGraphicsBeginImageContext(size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(self.cgColor)
        context?.fill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
    }

    static var colorE7E6EC: UIColor {
        get {
            return UIColor(0xE7E6EC)
        }
    }

    static var color007CBD: UIColor {
        get {
            return UIColor(0x007CBD)
        }
    }
}

