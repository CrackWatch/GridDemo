//
//  Model.swift
//  GridDemo
//
//  Created by MicroProgram_G6107009 on 2018/3/28.
//  Copyright © 2018年 Lazy. All rights reserved.
//

import Foundation
import ObjectMapper

class A: Mappable {
    var b: [[B]] = [[]]

    required init?(map: Map) {

    }

    func mapping(map: Map) {
        b <- map["firstDay"]
    }
}

class B: Mappable {
    var title: String = ""

    required init?(map: Map) {

    }


    func mapping(map: Map) {
        title <- map["title"]
    }
}


class MainModle: Mappable {

    var success: String = ""
    var firstday: [[Model]] = [[]]

    required init?(map: Map) {

    }

    func mapping(map: Map) {
        success <- map["success"]
        firstday <- map["firstday"]
    }
}


class Model: Mappable {
    var isshow:Bool?
    var name: String = ""
    var date: String = ""
    var info: String = ""
    var stage: String = ""
    var website: String = ""
    var facebook: String = ""
    var youtube: String = ""
    var image: String = ""
    var duration: Int = 0
    var start: String = ""
    var end: String = ""
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        isshow <- map["isshow"]
        name <- map["name"]
        date <- map["date"]
        info <- map["info"]
        stage <- map["stage"]
        website <- map["website"]
        facebook <- map["facebook"]
        youtube <- map["youtube"]
        image <- map["image"]
        duration <- map["duration"]
        start <- map["start"]
        end <- map["end"]

    }
}
