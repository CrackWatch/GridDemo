//
//  Api.swift
//  GridDemo
//
//  Created by MicroProgram_G6107009 on 2018/3/28.
//  Copyright © 2018年 Lazy. All rights reserved.
//

import Foundation
import ObjectMapper
import RxSwift
import Moya
import Moya_ObjectMapper

class Api {
    let provider = MoyaProvider<Service>()

    func bandInfo() -> Observable<[Model]> {
        return provider.rx.request(.getBandInfo).asObservable().mapArray(Model.self)
    }

    func firstDay() -> Observable<MainModle> {
        return provider.rx.request(.getFirstDay).asObservable().mapObject(MainModle.self)
    }
}
