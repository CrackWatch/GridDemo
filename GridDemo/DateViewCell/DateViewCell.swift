//
//  DateViewCell.swift
//  GridDemo
//
//  Created by Lazy on 2018/3/27.
//  Copyright © 2018年 Lazy. All rights reserved.
//

import G3GridView

class DateViewCell: GridViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var borderView: UIView!

    static var nid: UINib {
        return UINib(nibName: "DateViewCell", bundle: Bundle(for: self))
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    
        timeLabel.textColor = .white
        timeLabel.numberOfLines = 1
        timeLabel.textAlignment = .left
    }

    func configDateViewCell(time: String) {
        timeLabel.text = time
    }
}
