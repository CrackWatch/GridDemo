//
//  ViewController.swift
//  GridDemo
//
//  Created by Lazy on 2018/3/27.
//  Copyright © 2018年 Lazy. All rights reserved.
//

import UIKit
import G3GridView
import RxCocoa
import RxSwift

class ViewController: UIViewController {

    @IBOutlet weak var stageView: GridView!
    @IBOutlet weak var timeView: GridView!
    @IBOutlet weak var bandView: GridView!


    let viewModel: ViewModel = ViewModel()
    let bag: DisposeBag! = DisposeBag()
    let stages: [String] = ["鬼島", "射日", "澄波", "煙囪", "春萌", "向生", "七虎"]
    let times: [String] = ["12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"]


    lazy var stageViewCellDataSource: StageViewDataSource = .init(stages: self.stages)
    lazy var dateViewCellDataSource: TimeViewDataSource = .init(times: self.times)
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor(0x002b70)
        view.layoutIfNeeded()
        viewModel.getFirst().subscribe(onNext: { [weak self] (result) in
            guard let w = self else { return }
            if result {
                w.bandView.reloadData()
                w.stageView.reloadData()

            }
        }).disposed(by: bag)



        //註冊
        bandView.register(BandViewCell.nib, forCellWithReuseIdentifier: "BandViewCell")
        stageView.register(StageViewCell.nib, forCellWithReuseIdentifier: "StageViewCell")
        timeView.register(DateViewCell.nid, forCellWithReuseIdentifier: "DateViewCell")

        //band

        bandView.layoutWithoutFillForCell = true
        bandView.superview?.clipsToBounds = true
        bandView.contentInset.top = stageView.bounds.height
        //最小縮放值
        bandView.minimumScale = Scale(x: 0.6, y: 0.6)
        //最大縮放值
        bandView.maximumScale = Scale(x: 1.5, y: 1.5)
        bandView.scrollIndicatorInsets.top = bandView.contentInset.top
        bandView.scrollIndicatorInsets.left = timeView.bounds.width
        bandView.isInfinitable = false
        bandView.delegate = self
        bandView.dataSource = self
        bandView.reloadData()

        //stage
        stageView.isInfinitable = false
        stageView.superview?.isUserInteractionEnabled = false
        stageView.minimumScale.x = bandView.minimumScale.x
        stageView.maximumScale.x = bandView.maximumScale.x
        stageView.delegate = stageViewCellDataSource
        stageView.dataSource = stageViewCellDataSource
        stageView.reloadData()
        //time
        timeView.superview?.clipsToBounds = true

        timeView.superview?.isUserInteractionEnabled = false
        timeView.contentInset.top = stageView.bounds.height
        timeView.minimumScale.y = bandView.minimumScale.y
        timeView.maximumScale.y = bandView.maximumScale.y
        timeView.isInfinitable = false
        timeView.delegate = dateViewCellDataSource
        timeView.dataSource = dateViewCellDataSource
        timeView.reloadData()

    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate(alongsideTransition: { _ in
            self.timeView.invalidateContentSize()
            self.stageView.invalidateContentSize()
            self.view.layoutIfNeeded()
        })
    }
}

extension ViewController: GridViewDelegate, GridViewDataSource {

    func numberOfColumns(in gridView: GridView) -> Int {
        return viewModel.model.value.count
    }

    func gridView(_ gridView: GridView, numberOfRowsInColumn column: Int) -> Int {
        return viewModel.model.value[column].count
    }

    func gridView(_ gridView: GridView, cellForRowAt indexPath: IndexPath) -> GridViewCell {
        let cell = gridView.dequeueReusableCell(withReuseIdentifier: "BandViewCell", for: indexPath) as! BandViewCell
        cell.configBandViewCell(model: viewModel.model.value[indexPath.column][indexPath.row])
        return cell
    }

    func gridView(_ gridView: GridView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(viewModel.model.value[indexPath.column][indexPath.row].duration * 2)
    }

    func gridView(_ gridView: GridView, didSelectRowAt indexPath: IndexPath) {
        NSLog("\(indexPath)")
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        stageView.contentOffset.x = scrollView.contentOffset.x
        timeView.contentOffset.y = scrollView.contentOffset.y
    }

    func gridView(_ gridView: GridView, didScaleAt scale: CGFloat) {
        stageView.contentScale(scale)
        timeView.contentScale(scale)
    }

}

final class TimeViewDataSource: NSObject, GridViewDelegate, GridViewDataSource {

    var times: [String] = []

    init(times: [String]) {
        self.times = times
    }

    func gridView(_ gridView: GridView, numberOfRowsInColumn column: Int) -> Int {
        return 12
    }

    func gridView(_ gridView: GridView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60 * 2
    }

    func gridView(_ gridView: GridView, cellForRowAt indexPath: IndexPath) -> GridViewCell {
        let cell = gridView.dequeueReusableCell(withReuseIdentifier: "DateViewCell", for: indexPath) as! DateViewCell
        cell.configDateViewCell(time: times[indexPath.row])
        return cell
    }


}

final class StageViewDataSource: NSObject, GridViewDataSource, GridViewDelegate {

    let stages: [String]
    init(stages: [String]) {
        self.stages = stages
    }

    func numberOfColumns(in gridView: GridView) -> Int {
        return stages.count
    }

    func gridView(_ gridView: GridView, numberOfRowsInColumn column: Int) -> Int {
        return 1
    }

    func gridView(_ gridView: GridView, cellForRowAt indexPath: IndexPath) -> GridViewCell {
        let cell = gridView.dequeueReusableCell(withReuseIdentifier: "StageViewCell", for: indexPath) as! StageViewCell
        cell.configStageCell(stageName: stages[indexPath.column])

        return cell
    }
}

