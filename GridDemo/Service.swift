//
//  Service.swift
//  GridDemo
//
//  Created by MicroProgram_G6107009 on 2018/3/28.
//  Copyright © 2018年 Lazy. All rights reserved.
//

import Foundation
import Moya

enum Service {
    case getBandInfo
    case getFirstDay
}

extension Service: TargetType {

    var baseURL: URL {
        return URL(string: "https://gitlab.com/wakeuparts/api/raw/master/")!
    }

    var path: String {
        switch self {
        case .getBandInfo:
            return "WakeupJson.json"
        case .getFirstDay:
            return "timetable.json"
        }
    }

    var method: Moya.Method {
        switch self {
        case .getBandInfo:
            return .get
        case .getFirstDay:
            return .get
        }
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        switch self {
        case .getBandInfo:
            return .requestPlain
        case .getFirstDay:
            return .requestPlain
        }
    }

    var headers: [String: String]? {
        return nil
    }
    
}

