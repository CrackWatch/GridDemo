//
//  StageViewCell.swift
//  GridDemo
//
//  Created by Lazy on 2018/3/27.
//  Copyright © 2018年 Lazy. All rights reserved.
//

import G3GridView

class StageViewCell: GridViewCell {

    @IBOutlet weak var stageLabel: UILabel!
    
    static var nib: UINib {
        return UINib(nibName: "StageViewCell", bundle: Bundle(for: self))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        stageLabel.textColor = UIColor(0xFFFFFF)
        stageLabel.textAlignment = .center
    }

    func configStageCell(stageName: String) {
        stageLabel.text = stageName
    }
}
