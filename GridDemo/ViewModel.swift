//
//  ViewModel.swift
//  GridDemo
//
//  Created by MicroProgram_G6107009 on 2018/3/28.
//  Copyright © 2018年 Lazy. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class ViewModel {

    var model = BehaviorRelay<[[Model]]>(value: [[]])
    var bag: DisposeBag! = DisposeBag()
    let api = Api()
    let queue = SerialDispatchQueueScheduler.init(qos: .background)

    func getBand() -> Observable<Bool> {
        return api.bandInfo().flatMap { [weak self] (result) -> Observable<Bool> in
            guard let w = self else { return Observable.just(false) }
            dump(result)
            return Observable.just(true).subscribeOn(w.queue).observeOn(MainScheduler.instance)
        }
    }

    func getFirst() -> Observable<Bool> {
        return api.firstDay().flatMap({ [weak self] (result) -> Observable<Bool> in
            guard let w = self else { return Observable.just(false) }
            w.model.accept(result.firstday)
            
            return Observable.just(true).subscribeOn(w.queue).observeOn(MainScheduler.instance)
        })
    }
}
